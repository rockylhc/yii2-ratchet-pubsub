<?php

namespace lazybonez\pubsub;


use yii\web\AssetBundle;
use yii\web\View;

class PubSubAsset extends AssetBundle
{

    public $js = [
        'js/script.js'
    ];
    public $depends = [
        'yii\web\YiiAsset',
    ];

    public function init() {
        $this->sourcePath = __DIR__;
            $this->jsOptions['position'] = View::POS_END;
        if (!YII_DEBUG) {
            $this->js = ['js/script.min.js'];
        }
        parent::init();
    }
}