lazybonez = window.lazybonez || {};
var PubSub = (function(){
    var conn = null;
    SubscribeChannel = function(i, callback){
        var that = this;
        that.getConnection().onopen = function(e) {
            that.getConnection().send(JSON.stringify({command: "subscribe", channel: i}));
        };
    }

    openConnection  = function(){
        conn = new WebSocket('ws://localhost:443');
        return conn;
    }

    getConnection = function(){
        if(!conn){
            this.openConnection();
        }
        return conn;
    }

    waitForConnection = function (callback, interval) {
        if (this.getConnection().readyState === 1) {
            callback();
        } else {
            var that = this;
            // optional: implement backoff for interval here
            setTimeout(function () {
                that.waitForConnection(callback, interval);
            }, interval);
        }
    };

    publishMsg  = function(msg){
        this.waitForConnection(function(){
            this.getConnection().send(JSON.stringify({command: "message", message: msg}));
            if (typeof callback !== 'undefined') {
                callback();
            }
        }, 1000);

        this.getConnection().onmessage = function(e) {
            alert(e.data+ ' has connected');
        };
    }
    return{
        SubscribeChannel:SubscribeChannel,
        getConnection:getConnection,
        openConnection:openConnection,
        publishMsg:publishMsg,
        waitForConnection:waitForConnection
    }
})();


lazybonez.pubsub = {
    getChannel : function(){
        $.ajax({
            url: '/todo/get',
            data: {
                email: Cookies.get('email'),
                accessToken: Cookies.get('accessToken')
            },
            success:function(d){
                if(d.code==200){
                    var channelID;
                    var htmlString = '<ul>';
                    for (var i=0; i < d.todo.length; i++){
                        channelID = d.todo[i].guid;
                        htmlString += '<li>';
                        htmlString += '<a href="/todo/#/'+d.todo[i].guid+'" class="btn btn-default todo" data-id="'+d.todo[i].id+'">'+ d.todo[i].content +'</a>';
                        htmlString += '</li>';
                    }
                    $('.todo-overview').html(htmlString);

                }else if(d.code == 403){

                    $.ajax({
                        type: 'POST',
                        url: '/user/logout',
                        success: function(d) {
                            if (d.code == 200) {
                                Cookies.remove('email');
                                Cookies.remove('accessToken');
                            }
                        }
                    });

                }else{
                    console.log('get channel');
                }
            }
        });
    },
    connectChannel: function(id){
        PubSub.SubscribeChannel(id);

        $.ajax({
            url:'/todo',
            data:{
                id:id
            },
            success:function(d){
                if(d.code == 200){
                    console.log(d);
                }
            }
        });
    },
    publishMsg: function(msg){
        PubSub.publishMsg(msg);
    }
}

$(function(){

    $('.todo-overview').on('click','.todo',function(evt){
        evt.preventDefault();
        channelId = $(this).attr('data-id');
        todoHref = $(this).attr('href');
        hashHref = todoHref.split(/#\//);
        lazybonez.pubsub.connectChannel(hashHref[1]);
        lazybonez.pubsub.publishMsg(Cookies.get('email'));
    });

});