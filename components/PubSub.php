<?php

namespace lazybonez\pubsub\components;
use Ratchet\ConnectionInterface;
use Ratchet\Wamp\Exception;
use Yii;
use yii\helpers\Json;
use Ratchet\MessageComponentInterface;

class PubSub implements MessageComponentInterface
{
    protected $clients = [];
    protected $subscriptions = [];
    protected $users;


    public function __construct() {
        $this->clients = new \SplObjectStorage;
        $this->subscriptions = [];
        $this->users = [];
    }

    public function onOpen(ConnectionInterface $conn)
    {
        $this->clients->attach($conn);
        $this->users[$conn->resourceId] = $conn;
        echo 'New connection: '.$conn->resourceId."\n";
    }

    public function onMessage(ConnectionInterface $conn, $msg)
    {
        $data = json_decode($msg);

        switch ($data->command) {
            case "subscribe":
                $this->subscriptions[$conn->resourceId] = $data->channel;
                echo "Subscribe channel: ".$data->channel."\n";
                break;

            case "connect":
                echo 'message resourceId: '.$this->subscriptions[$conn->resourceId]."\n";
                if (isset($this->subscriptions[$conn->resourceId])) {
                    $target = $this->subscriptions[$conn->resourceId];
                    foreach ($this->subscriptions as $id=>$channel) {
                        if ($channel == $target && $id != $conn->resourceId) {
                            $this->users[$id]->send($msg);
                        }
                    }
                }
            break;
            case "todo":
                if (isset($this->subscriptions[$conn->resourceId])) {
                    $target = $this->subscriptions[$conn->resourceId];
                    foreach ($this->subscriptions as $id=>$channel) {
                        if ($channel == $target && $id != $conn->resourceId) {
                            $this->users[$id]->send($msg);
                        }
                    }
                }
            break;

            case "edittodo":
                if (isset($this->subscriptions[$conn->resourceId])) {
                    $target = $this->subscriptions[$conn->resourceId];
                    foreach ($this->subscriptions as $id=>$channel) {
                        if ($channel == $target && $id != $conn->resourceId) {
                            $this->users[$id]->send($msg);
                        }
                    }
                }
            break;
        }
    }



    public function onClose(ConnectionInterface $conn)
    {
        $this->clients->detach($conn);
        unset($this->users[$conn->resourceId]);
        unset($this->subscriptions[$conn->resourceId]);
        echo "Connection dropped: ".$conn->resourceId."\n";
    }
    public function onError(ConnectionInterface $conn, \Exception $e)
    {
        echo "An error has occurred: {$e->getMessage()}\n";
        $conn->close();
    }

}