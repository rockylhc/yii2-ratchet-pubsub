Ratchet Pubsub component
========================
An Yii2 extension using Ratchet socket component

Installation
------------

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Either run

```
php composer.phar require --prefer-dist lazybonez/pubsub "*"
```

or add

```
"lazybonez/pubsub": "*"
```

to the require section of your `composer.json` file.


Usage
-----

Once the extension is installed, simply use it in your code by  :

```php
<?= \lazybonez\component\AutoloadExample::widget(); ?>```